## 0.2.5 - 2024-05-21
* Specify non-imprecise dependency requirements

## 0.2.4 - 2024-02-04
* Support lockfileVersion 3
* Update dependencies

## 0.2.3 - 2022-10-06
* Fix handling of local "file:{name}" dependencies
* Update dependencies

## 0.2.2 - 2022-01-14
* Update dependencies
* Bump MSRV to 1.56, use 2021 edition
* Publish binaries in more architectures on GitLab

## 0.2.1 - 2021-09-26
* Update dependencies
* Publish amd64 and armv7 binaries on GitLab

## 0.2.0 - 2021-09-05
* Add support for lockfileVersion 2
* Check for package named "-"
* Don't abort on file read/JSON errors

## 0.1.1 - 2021-05-20
* Process all files before exiting

## 0.1.0 - 2021-05-19
* Initial release
